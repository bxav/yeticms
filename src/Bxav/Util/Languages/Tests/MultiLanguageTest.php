<?php

namespace Bxav\Util\Languages\Tests;

use Bxav\Util\Languages\MultiLanguage;
use Bxav\Util\Languages\MultiLanguageString;

class MultiLanguageTest extends \PHPUnit_Framework_TestCase
{
    protected $multiLanguage = null;

    public function setUp() {
        $this->multiLanguage = new MultiLanguage(array(
            "languages" => array("en", "fr"), 
            "local" => "en"
        ));
    }

    public function testCreation() {
        $this->assertEquals('en', $this->multiLanguage->getLocal());
        $this->assertTrue($this->multiLanguage->isValidLanguage("fr"));
        $this->assertTrue($this->multiLanguage->isValidLanguage("en"));
        $this->assertFalse($this->multiLanguage->isValidLanguage("de"));
    }
    
    /**
     * @expectedException Exception
     * @expectedExceptionMessage Local not found
     */
    public function testGetPositionWithWrongIdContent() {
        $this->multiLanguage->setLocal("es");
    }
    
    public function testSetValidLocal() {
        $this->multiLanguage->setLocal("fr");
        $this->assertEquals('fr', $this->multiLanguage->getLocal());
    }
    
    public function testGetMultiLanguageString() {
        $this->assertInstanceOf("Bxav\Util\Languages\MultiLanguageString", $this->multiLanguage->getMultiLanguageString());
    }
    
    /**
     * @expectedException Exception
     * @expectedExceptionMessage Error tag name
     */
    public function testGetNewMultiLanguageStringWithErrorTagName() {
        $dom = new \DOMDocument('1.0');
        $dom->loadXML("<error><en>home</en><fr>accueil</fr></error>");
        $stringMultiLang = $this->multiLanguage->getInitMultiLanguageString($dom->firstChild);
    }
    
    public function testGetNewMultiLanguageStringWithDOMElement() {
        $dom = new \DOMDocument('1.0');
        $dom->loadXML("<multilanguagestring><en>home</en><fr>accueil</fr></multilanguagestring>");
        $stringMultiLang = $this->multiLanguage->getInitMultiLanguageString($dom->firstChild);
        
        $this->assertInstanceOf("Bxav\Util\Languages\MultiLanguageString", $stringMultiLang);
        $this->assertEquals('home', $stringMultiLang);
        
        $this->multiLanguage->setLocal("fr");
        $this->assertInstanceOf("Bxav\Util\Languages\MultiLanguageString", $stringMultiLang);
        $this->assertEquals('accueil', $stringMultiLang);
    }
    
    public function testSetMultiLanguageStringWithDOMElement() {
        $dom = new \DOMDocument('1.0');
        $dom->loadXML("<multilanguagestring><en>home</en><fr>accueil</fr></multilanguagestring>");
        $stringMultiLang = $this->multiLanguage->getMultiLanguageString();
        
        $this->multiLanguage->getSetMultiLanguageString($stringMultiLang, $dom->firstChild);
        $this->assertInstanceOf("Bxav\Util\Languages\MultiLanguageString", $stringMultiLang);
        $this->assertEquals('home', $stringMultiLang);
        
        $this->multiLanguage->setLocal("fr");
        $this->assertInstanceOf("Bxav\Util\Languages\MultiLanguageString", $stringMultiLang);
        $this->assertEquals('accueil', $stringMultiLang);
        
    }
            
    public function testGetDOMElementWithMultiLanguageString() {
        $stringMultiLang = $this->multiLanguage->getMultiLanguageString();
        $this->multiLanguage->setLocal("en");
        $stringMultiLang->set("home");
        $this->multiLanguage->setLocal("fr");
        $stringMultiLang->set("accueil");
        
        $dom = new \DOMDocument('1.0');
        $element = $dom->appendChild($this->multiLanguage->getDOMElementWithLanguages($stringMultiLang, $dom));
        $expectString = "<multilanguagestring><en>home</en><fr>accueil</fr></multilanguagestring>";
        
        $this->assertFalse(!strpos($dom->saveXML(), $expectString));
    }
}