<?php

namespace Bxav\Util\Languages\Tests;

use Bxav\Util\Languages\MultiLanguageString;

class MultiLanguageStringTest extends \PHPUnit_Framework_TestCase
{
    protected $multiLanguageString = null;
    
    protected $multiLanguageMock = null;

    public function setUp() {
        $this->multiLanguageMock = $this->getMultiLanguageMock("en");
        $this->multiLanguageString = new MultiLanguageString($this->multiLanguageMock);
    }

    public function testCreation() {
        $this->assertEquals('', $this->multiLanguageString);
    }
    
    public function testInitContent() {
        $this->multiLanguageString->set("home");
        $this->assertEquals('home', $this->multiLanguageString);
    }
    
    public function testSetContentNewLanguage() {
        $this->multiLanguageString->set("home");
        $this->multiLanguageMock->setLocal("fr");
        $this->multiLanguageString->set("accueil");
        $this->assertEquals('accueil', $this->multiLanguageString);
        $this->multiLanguageMock->setLocal("en");
        $this->assertEquals('home', $this->multiLanguageString);
    }  
        
    public function testCreationWithArray() {
        $multiLanguageString = new MultiLanguageString($this->multiLanguageMock, array("en" => "home", "fr" => "accueil"));
        $this->multiLanguageMock->setLocal("fr");
        $this->assertEquals('accueil', $multiLanguageString);
        $this->multiLanguageMock->setLocal("en");
        $this->assertEquals('home', $multiLanguageString);
    }
    
    private function getMultiLanguageMock($local) {
        $mock = new MultiLanguageMock();
        $mock->setLocal($local);
        return $mock;
    }  
}

class MultiLanguageMock 
{
    protected $local;
    public function getLocal() {return $this->local;}
    public function setLocal($local) {$this->local = $local;}
}
