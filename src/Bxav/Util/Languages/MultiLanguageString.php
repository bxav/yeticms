<?php

namespace Bxav\Util\Languages;

class MultiLanguageString
{
    private $multiLanguageService;    
    private $content = array();
    
    public function __construct($multiLanguageService, $content = '') {
        $this->multiLanguageService = $multiLanguageService;
        $this->set($content);
    }
    
    public function set($content) {
        if(is_array($content)) {
            $this->content = $content; 
        }
        else {
            $this->content[$this->multiLanguageService->getLocal()] = $content;
        }
    }
    
    public function get() {
        return $this->content;
    }
    
    public function __toString() {
        return $this->content[$this->multiLanguageService->getLocal()];
    } 
}