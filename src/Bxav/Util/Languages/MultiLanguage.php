<?php

namespace Bxav\Util\Languages;

use Bxav\Util\Languages\MultiLanguageString;

class MultiLanguage
{
    const TAG_NAME = "multilanguagestring";
    
    private $languages = array("en");

    private $local = 'en';
    
    public function __construct($parrams = array()) {
        if(isset($parrams["languages"])) $this->languages = $parrams["languages"];
        if(isset($parrams["local"])) $this->local = $parrams["local"];
    }
    
    public function setLocal($local) {
        if(!$this->isValidLanguage($local)) {
            throw new \Exception("Local not found");   
        } 
        $this->local = $local;
    }
    
    public function getLocal() {
        return $this->local;
    }
    
    public function getMultiLanguageString() {
        return new MultiLanguageString($this);
    }
    
    public function getInitMultiLanguageString(\DomElement $domLanguages) {
        $languages = $this->getArrayLanguageWithDomElement($domLanguages);
        return new MultiLanguageString($this, $languages);
    }
    
    public function getSetMultiLanguageString(MultiLanguageString $stringMultiLang, \DomElement $domLanguages) {
        $languages = $this->getArrayLanguageWithDomElement($domLanguages);
        return $stringMultiLang->set($languages);
    }
    
    public function getDOMElementWithLanguages(MultiLanguageString $stringMultiLang, \DomDocument $dom) {
        $element = $dom->createElement(MultiLanguage::TAG_NAME);
        foreach($stringMultiLang->get() as $language => $value) {
            $element->appendChild(new \DomElement($language, $value));
        }
        return $element;
    }
    
    public function isValidLanguage($language) {
        return $this->findValueInArray($language, $this->languages);
    }
    
    private function getArrayLanguageWithDomElement(\DomElement $domLanguages) {
        if($domLanguages->tagName != MultiLanguage::TAG_NAME) {
            throw new \Exception("Error tag name");
        }
        $listDomElement = $domLanguages->childNodes;
        $languages = array();
        foreach($listDomElement as $item) {
            $languages[$item->nodeName] = $item->nodeValue; 
        }
        return $languages;
    }
    
    private function findValueInArray($value, $array) {
        foreach($array as $val) {
            if($value == $val) return true; 
        }
        return false;
    }
}