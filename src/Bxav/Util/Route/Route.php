<?php

namespace Bxav\Util\Route;

class Route
{
    private $name;
    private $params;
    
    static public function creationWithNameRouteAndParams($name, $params) {
        return new Route($name, $params);
    }
    
    private function __construct($name, $params) {
        $this->name = $name;
        $this->params = $params;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function getParams() {
        return $this->params;
    }
}