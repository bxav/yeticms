<?php

namespace Bxav\Util\Route\Tests;

use Bxav\Util\Route\Route;

class RouteTest extends \PHPUnit_Framework_TestCase
{
    public function testCreation() {
        $route = Route::creationWithNameRouteAndParams('name', array('param1', 'param2'));
 
        $this->assertEquals('name', $route->getName());
        $this->assertEquals(array('param1', 'param2'), $route->getParams());
    }
    
}