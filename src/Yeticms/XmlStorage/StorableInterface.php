<?php

namespace Yeticms\XmlStorage;

use Bxav\Util\Languages\MultiLanguage;

/**
 * @author Xavier b <xavier.b@zoho.com>
 */
interface StorableInterface
{
    public function setDomElement(\DomElement $element);
    public function init($deep = false);
    public function updateDomElement($deep = false);
}


  