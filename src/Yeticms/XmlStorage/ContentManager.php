<?php

namespace Yeticms\XmlStorage;

use Yeticms\Model\ContentManager as ContentManagerBase;
    
/**
 * @author Xavier b <xavier.b@zoho.com>
 */
class ContentManager extends ContentManagerBase
{   
    private $domDocument = null;

    public function __construct(array $param) {
        if(isset($param["document"]) && ($param["document"] instanceof \DomDocument)) $this->domDocuments = $param["document"]; 
        parent::__construct($param);
    }

    public function getContentById($id) {
        if(is_null($this->domDocuments)) {
            throw new \Exception("no document associated");
        }
        $element = $this->domDocuments->getElementById($id);
        $content = $this->createContent($element->getAttribute("type"));
        return $content;
    }    
}