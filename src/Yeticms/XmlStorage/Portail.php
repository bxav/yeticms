<?php

namespace Yeticms\XmlStorage;

use Yeticms\Model\Portail as PortailBase;
use Bxav\Util\Languages\MultiLanguage;

/**
 * @author Xavier b <xavier.b@zoho.com>
 */
class Portail extends PortailBase
{   
    const NAME_TAG = "portail";
    
    private $multiLanguage;
    
    private $domElement;
    
    public function __construct(MultiLanguage $multiLanguage) {
        $this->setMultiLanguage($multiLanguage);
    }
    
    protected function chargeById($id) {  
        $portail = new Portail($this->multiLanguage);
        $domElement = $this->domElement->ownerDocument->getElementById($id);
        if($domElement->parentNode !== $this->domElement) 
            throw new \Exception("Id not valid");
        $portail->setDomElement($domElement);
        $portail->init();
        return $portail;
    }
    
    public function setMultiLanguage(MultiLanguage $multiLanguage) {
        $this->multiLanguage = $multiLanguage;
    }
    
    public function setDomElement(\DomElement $domElement) {
        if($domElement->tagName !== Portail::NAME_TAG) 
            throw new \Exception("Not a valid DomElement"); 
        $this->domElement = $domElement;
    }
    
    public function init($deep = false) {
        $lazyLoad = !$deep;
        $this->initTree($lazyLoad);
    }
    
    public function updateDomElement($deep = false) {
        $this->domElement->setAttribute("id", $this->getID());
        if($deep) {
            $portails = $this->getChildPortails();
            foreach($portails as $item) {
                $item->updateDomElement();
            }
        }
    }
    
    private function initTree($lazyLoad = true) {
        if(!($this->domElement instanceof \DomElement)) {
            throw new \Exception("DomElement not set");
        }
        $this->setMembers();
        $domPortails = $this->domElement->getElementsByTagName(Portail::NAME_TAG);
        $portails = array();
        foreach($domPortails as $item) {
            if($item->parentNode === $this->domElement) {
                $portailChild = Null;
                if($lazyLoad == false) {
                    $portailChild = new Portail($this->multiLanguage);
                    $portailChild->setDomElement($item);
                    $portailChild->init(true);
                }
                else {
                    $portails[$item->getAttribute("id")] = Null;
                }
                $portails[$item->getAttribute("id")] = $portailChild;
            }
        }
        $this->setChildPortails($portails);
    }
    
    private function setMembers() {
        $this->setId($this->domElement->getAttribute("id"));
        $this->setTitle($this->multiLanguage->getInitMultiLanguageString($this->getFirstElementByTag("title")->firstChild));
    }
    
    private function getFirstElementByTag($tag) {
        $element = $this->domElement->getElementsByTagName($tag)->item(0);
        if(is_null($element) || $element->parentNode !== $this->domElement) {
            return null;
        }
        else {
            return $element;
        }
    }
}

  