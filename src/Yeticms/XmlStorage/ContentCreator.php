<?php

namespace Yeticms\XmlStorage;

use Yeticms\Model\ContentCreator as ContentCreatorBase;
    
/**
 * @author Xavier b <xavier.b@zoho.com>
 */
abstract class ContentCreator extends ContentCreatorBase
{
}

