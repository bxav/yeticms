<?php

namespace Yeticms\XmlStorage;

use Yeticms\Model\Content as ContentBase;

/**
 * @author Xavier b <xavier.b@zoho.com>
 */
abstract class Content extends ContentBase implements StorableInterface
{
    private $domElement;
    
    public function setDomElement(\DomElement $element) {
        $this->domElement = $element;
    }
    
    public function init($deep = false) {
        $this->initContent($deep);
    }
    
    public function updateDomElement($deep = false) {}
    
    protected function getDomElement() {
        return $this->domElement;
    }
    
    protected abstract function initContent($deep);
    
    protected function getFirstElementByTag($tag) {
        $element = $this->domElement->getElementsByTagName($tag)->item(0);
        if(is_null($element) || $element->parentNode !== $this->domElement) {
            return null;
        }
        else {
            return $element;
        }
    }
}
