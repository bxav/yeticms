<?php

/**
 * @author Xavier b <xavier.b@zoho.com>
 */

namespace Yeticms\BackEnd;

use Silex\Application;
use Silex\ControllerProviderInterface;

class BackEndControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        $controllers->get('/',  function () use ($app) {
            return $app['twig']->render('index.html', array());
        })
        ->bind('homepage_admin')
        ;
        return $controllers;
    }
}
