<?php

namespace Yeticms\Util\Template;

use Yeticms\Model\Content;
    
/**
 * @author Xavier b <xavier.b@zoho.com>
 */
abstract class Template
{
    protected $templateEngine;
    
    public function setTemplateEngine($templateEngine) {
        $this->templateEngine = $templateEngine;
    }
    
    public abstract function render(Content $content, $admin = false);
}