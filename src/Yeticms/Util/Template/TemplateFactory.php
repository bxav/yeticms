<?php

namespace Yeticms\Util\Template;

use Yeticms\Util\Template\Template;
    
/**
 * @author Xavier b <xavier.b@zoho.com>
 */
class TemplateFactory
{
    private $templates = array();
    
    private $templateEngine;
    
    public function __construct(array $templates, $templateEngine) {
        $this->templates = $templates;
        $this->templateEngine = $templateEngine;
        $this->initTemplates();
    }
    
    public function getTemplate($templateKey) {
        try {
            return $this->templates[$templateKey];
        }
        catch(\Exception $e) {
            throw new \Exception("Template Key not registered");
        }
    }
    
    private function initTemplates() {
        foreach($this->templates as $template) {
            if(!($template instanceof Template)) {
                throw new \Exception("Error interface template");
            }
            $template->setTemplateEngine($this->templateEngine);
        }
    }
}