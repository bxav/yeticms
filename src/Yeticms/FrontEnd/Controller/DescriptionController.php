<?php

/**
 * @author Xavier b <xavier.b@zoho.com>
 */

namespace Yeticms\FrontEnd\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Yeticms\Model\DescriptionModule;
use Yeticms\Model\ContentDescription;

class DescriptionController
{
 
    public function indexAction(Request $request, Application $app)
    {
        return $app['twig']->render('index.html', array());
    }
    
    public function visuAction(Request $request, Application $app, $pageId)
    {
        $contentDescription = $app['description.module']->findContentDescriptionByIdPage($pageId);
        return $app['twig']->render('frontend/description/visu.html', array(
            'title' => $contentDescription->getTitle(),
            'bodyContent' => $contentDescription->getBodyContent()
        ));
    }
}