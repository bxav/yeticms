<?php

/**
 * @author Xavier b <xavier.b@zoho.com>
 */

namespace Yeticms\FrontEnd;

use Silex\Application;
use Silex\ControllerProviderInterface;

class FrontEndControllerProvider implements ControllerProviderInterface
{
    private $controllers;
    
    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];
        $this->controllers->get('/{pageId}',  'Yeticms\FrontEnd\Controller\DescriptionController::visuAction')
        ->assert('pageId', '\d+')
        ->bind('description');
        
        $this->controllers->get('/',  'Yeticms\FrontEnd\Controller\DescriptionController::indexAction');
        $this->controllers->get('/test',  function() {
            return 'test!';
        })
        ->bind('test')
        ;
       
        return $this->controllers;
    }
}
