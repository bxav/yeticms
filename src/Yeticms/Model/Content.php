<?php

namespace Yeticms\Model;
    
/**
 * @author Xavier b <xavier.b@zoho.com>
 */
abstract class Content
{
    private $id;
    
    public function setId($id) {
        $this->id = $id;
    } 
    
    public function getId() {
        return $this->id;
    }    
}


  