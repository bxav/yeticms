<?php

namespace Yeticms\Model;

use Yeticms\Model\Content;

/**
 * @author Xavier b <xavier.b@zoho.com>
 */
abstract class Portail
{   
    private $id = "";
    
    private $content;
    
    private $portails = array();
    
    private $title;
    
    public function setId($id) {
        $this->id = $id;
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function setTitle($title) {
        $this->title = $title;
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function addChildPortails(Portail $portail) {
        $this->portails[$portail->getId()] = $portail;
    }
    
    public function setChildPortails($portails) {
        $this->portails = $portails;
    }
    
    public function getChildPortails() {
        return $this->portails;
    }
    
    public function getChildPortailById($id) {
        if(!($this->portails[$id] instanceof Portail)) {
            $this->portails[$id] = $this->chargeById($id);   
        }
        return $this->portails[$id];
    }
    
    protected abstract function chargeById($id);
    
    public function addContent(Content $content) {
        $this->content = $content;
    }
    
    public function getContent() {
        return $this->content;        
    }
}

  