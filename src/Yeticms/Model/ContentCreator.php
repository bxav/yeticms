<?php

namespace Yeticms\Model;
    
/**
 * @author Xavier b <xavier.b@zoho.com>
 */
abstract class ContentCreator
{  
    private $dependencies = array();
    
    public function setDependencies($dependencies) {
        $dependencyNames = $this->getDepandancyNames();
        foreach($dependencyNames as $dependencyName) {
            try {
                $this->dependencies[$dependencyName] = $dependencies[$dependencyName];
                 
            }
            catch(\Exception $e) {
                throw new \Exception("Error dependency:".$dependencyName);
            }
        }
    }
    
    protected abstract function getDepandancyNames();
    
    protected function getDependency($dependency) {
        try {
            return $this->dependencies[$dependency];
        }
        catch(\Exception $e) {
            throw new \Exception("Error dependency:".$dependency);
        }
    }
    
    
    
    public function create() {
        return $this->factoryMethod();
    }
    
    protected abstract function factoryMethod();
}

