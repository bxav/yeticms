<?php

namespace Yeticms\Model;

use Yeticms\Model\Content;
    
/**
 * @author Xavier b <xavier.b@zoho.com>
 */
class Page
{   
    private $content;

    public function setContent(Content $content) {
        $this->content = $content;
    }
    public function getContent() {
        return $this->content;
    }
}

  