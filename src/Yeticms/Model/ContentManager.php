<?php

namespace Yeticms\Model;

use Yeticms\Model\ContentCreator;
    
/**
 * @author Xavier b <xavier.b@zoho.com>
 */
abstract class ContentManager
{
    private $creators = array();
    private $dependencies = array();
    
    public function __construct(array $param) { 
        if(isset($param["dependencies"])) {
            $this->dependencies = $param["dependencies"]; 
        }
        if(isset($param["creators"])) {
            $this->creators = $param["creators"];
            $this->initCreatorsDependancies();
        }
    }
    
    public abstract function getContentById($id);
    
    public function createContent($type) {
        return $this->creators[$type]->create();
    }
    
    private function initCreatorsDependancies() {
        foreach($this->creators as $creator) {
            if(!($creator instanceof ContentCreator)) {
                throw new \Exception("Error interface creators");
            }
            $creator->setDependencies($this->dependencies);
        }
    }
    
    
}