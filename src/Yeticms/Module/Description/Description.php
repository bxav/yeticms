<?php

namespace Yeticms\Module\Description;

use Yeticms\Module\Description\DescriptionInterface;
use Bxav\Util\Languages\MultiLanguage;
use Yeticms\XmlStorage\Content;

/**
 * @author Xavier b <xavier.b@zoho.com>
 */
class Description extends Content implements DescriptionInterface
{
    private $text;

    private $multiLanguage;
    
    public function setText($text) {
        $this->text = $text;
    } 
    
    public function getText() {
        return $this->text;
    }
    
    protected function initContent($deep) {
        if(!($this->getDomElement() instanceof \DomElement)) {
            throw new \Exception("DomElement not set");
        }
        $this->setMembers();
    }
    
    public function setMultiLanguage(MultiLanguage $multiLanguage) {
        $this->multiLanguage = $multiLanguage;
    }
       
    protected function getMultiLanguage() {
        return $this->multiLanguage;
    }
    
    private function setMembers() {
        $this->setId($this->getDomElement()->getAttribute("id"));
        $this->setText($this->getMultiLanguage()->getInitMultiLanguageString($this->getFirstElementByTag("text")->firstChild));
    }
}


  