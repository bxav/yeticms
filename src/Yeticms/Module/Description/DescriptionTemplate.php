<?php

namespace Yeticms\Module\Description;

use Yeticms\Module\Description\Description;
    
/**
 * @author Xavier b <xavier.b@zoho.com>
 */
class DescriptionTemplate
{    
    public function render(Description $description, $admin = false) {
        $text = $description->getText();
        if(!$admin) {
            $templateName = "description/description.html";
        }
        else {
            $templateName = "admin/description/description.html";
        }
        
        return $this->templateEngine->render($templateName, array("text" => $text));
    }
}