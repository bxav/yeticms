<?php

namespace Yeticms\Module\Description;

/**
 * @author Xavier b <xavier.b@zoho.com>
 */
interface DescriptionInterface
{
    public function setText($text);
    
    public function getText(); 
}


  