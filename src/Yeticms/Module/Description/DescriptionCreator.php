<?php

namespace Yeticms\Module\Description;
    
use Yeticms\XmlStorage\ContentCreator;
use Yeticms\Module\Description\Description;

/**
 * @author Xavier b <xavier.b@zoho.com>
 */
class DescriptionCreator extends ContentCreator
{
    private static $depandancyNames = array("multilangue");
    
    protected function getDepandancyNames() {
        return self::$depandancyNames;
    }
    
    protected function factoryMethod() {
        $description = new Description();
        $description->setMultiLanguage($this->getDependency("multilangue"));
        return $description;        
    }
}
