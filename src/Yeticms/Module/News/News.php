<?php

namespace Yeticms\Module\News;

use Yeticms\Module\News\NewsInterface;
use Yeticms\XmlStorage\Content;

/**
 * @author Xavier b <xavier.b@zoho.com>
 */
class News extends Content implements NewsInterface
{
    protected function initContent($deep) {}
    
}


  