<?php

namespace Yeticms\Module\News;
    
use Yeticms\XmlStorage\ContentCreator;
use Yeticms\Module\News\News;

/**
 * @author Xavier b <xavier.b@zoho.com>
 */
class NewsCreator extends ContentCreator
{
    private static $depandancyNames = array("multilangue");
    
    protected function getDepandancyNames() {
        return self::$depandancyNames;
    }
    
    protected function factoryMethod() {
        return new News();
    }
}

