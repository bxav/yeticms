<?php

namespace Yeticms\Tests\Model;

use Yeticms\Model\Portail;

class PortailTest extends \PHPUnit_Framework_TestCase
{
    protected $portail = null;

    public function setUp() {
        $stub = $this->getMockPortail('website');
        $this->portail = $stub;
    }
    
    public function testCreation() {
        $this->assertEquals("website", $this->portail->getId());
        $this->assertEquals(array(), $this->portail->getChildPortails());
        $this->assertEmpty($this->portail->getContent());
    }
    
    public function testGetChildPortailsInArray() {
        $portailHome = $this->getMockPortail("home");
        $this->portail->addChildPortails($portailHome);
        $childPortails = $this->portail->getChildPortails();
        $this->assertEquals($portailHome, $childPortails["home"]);
    }
    
    public function testGetChildPortailById() {
        $portailHome = $this->getMockPortail("test");
        $this->portail->addChildPortails($portailHome);
        $childPortail = $this->portail->getChildPortailById("test");
        $this->assertEquals($portailHome, $childPortail);
    }
    
    public function testAddContent() {
        $oneContent = $this->getContent();
        $this->portail->addContent($oneContent);
        $this->assertEquals($oneContent, $this->portail->getContent());
    }
    
    private function getContent() {
        return $this->getMock("Yeticms\Model\Content");        
    }
    
    private function getMockPortail($id) {
        $stub = $this->getMockForAbstractClass('Yeticms\Model\Portail');
        $stub->setId($id);
        return $stub;
    }
    
}