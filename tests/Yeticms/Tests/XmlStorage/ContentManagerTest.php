<?php

namespace Yeticms\Tests\XmlStorage;

use Yeticms\XmlStorage\ContentManager;

class ContentManagerTest extends \PHPUnit_Framework_TestCase
{
   protected $xmlContentsString = '<?xml version="1.0"?>
                                        <!DOCTYPE contents [
                                            <!ELEMENT contents   (content+)>
                                            <!ATTLIST content    id           ID     #IMPLIED>
                                            ]>
                                        <contents>
                                            <content id="home" type="description">
                                                <description>
                                                </description>
                                            </content>
                                            <content id="about" type="description">
                                                <description>
                                                </description>
                                            </content>
                                            <content id="news" type="news">
                                                <news>
                                                </news>
                                            </content>
                                        </contents>';
    protected $contentManager;
    
    protected $contentCreatorMock;
    
    protected $dom = null;
    
    public function setUp() {
        $this->dom = new \DOMDocument();
        $this->dom->loadXML($this->xmlContentsString);
        $dependencies = array("multilangue" => $this->getMock("Bxav\Util\Languages\MultiLanguage"));
        $creators = array(
            "description" => $this->getMockCreator("Description"),
            "news" => $this->getMockCreator("News"),
        );
        $param = array("creators" => $creators,
                       "dependencies" => $dependencies,
                       "document" => $this->dom);
        $this->contentManager = new ContentManager($param);  
    }

    public function testCreationContent() {
        $content = $this->contentManager->createContent("description");
        $this->assertInstanceOf("Yeticms\XmlStorage\Content", $content);     
    }
    
    public function testGetContentById() {
        $expectedId = "news";
        $content = $this->contentManager->getContentById($expectedId);
        $this->assertInstanceOf("Yeticms\XmlStorage\Content", $content);
    }
    
    /**
     * @expectedException Exception
     */
    public function testCreateContentWithWrongType() {
        $content = $this->contentManager->createContent("bad_type");
        $this->assertInstanceOf("Yeticms\Model\Content", $content);                
    }
    
    private function getMockCreator($type) {
        $stub = $this->getMockForAbstractClass('Yeticms\XmlStorage\ContentCreator');
        $stub->expects($this->any())
                ->method('getDepandancyNames')
                ->will($this->returnValue(array()));
        $stub->expects($this->any())
                ->method('factoryMethod')
                ->will($this->returnValue($this->getMockForAbstractClass('Yeticms\XmlStorage\Content')));
        return $stub;
    }
}

