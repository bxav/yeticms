<?php

namespace Yeticms\Tests\XmlStorage;

use Yeticms\XmlStorage\Portail;
use Bxav\Util\Languages\MultiLanguage;

class PoretailTest extends \PHPUnit_Framework_TestCase
{
    protected $xmlPortailsString = '<?xml version="1.0"?>
                                        <!DOCTYPE portails [
                                            <!ELEMENT portails   (portail+)>
                                            <!ELEMENT portail    (title, portail)>
                                            <!ELEMENT title   (#PCDATA)>
                                            <!ATTLIST portail    id           ID     #IMPLIED>
                                            ]>
                                        <portails>
                                            <portail id="home">
                                                <title><multilanguagestring><en>home</en><fr>accueil</fr></multilanguagestring></title>
                                                <portail id="under-home1">
                                                    <title><multilanguagestring><en>home1</en><fr>accueil1</fr></multilanguagestring></title>
                                                    <portail id="underunder-home1">
                                                        <title><multilanguagestring><en>home1</en><fr>accueil1</fr></multilanguagestring></title>
                                                    </portail>
                                                </portail>
                                                <portail id="under-home2">
                                                    <title><multilanguagestring><en>home2</en><fr>accueil2</fr></multilanguagestring></title>
                                                </portail>
                                            </portail>
                                            <portail id="about">
                                                <title><multilanguagestring><en>about</en><fr>a propos</fr></multilanguagestring></title>
                                            </portail>
                                        </portails>';

    protected $multiLanguage = null;
    
    protected $portail = null;
    
    protected $dom = null;

    public function setUp() {
        $this->multiLanguage = new MultiLanguage(array("en", "fr"), "en");
        $this->dom = new \DOMDocument();
        $this->dom->loadXML($this->xmlPortailsString);
        $portail = new Portail($this->multiLanguage);
        $this->portail = $portail;
    }
    
    /**
     * @expectedException Exception
     * @expectedExceptionMessage Not a valid DomElement
     */
    public function testSetDomElementWithError() {
        $this->portail->setDomElement(new \DomElement("no_valid_name"));
    }
    
    public function testSetDomElement() {
        $this->portail->setDomElement(new \DomElement("portail"));
    }
    
    /**
     * @expectedException Exception
     * @expectedExceptionMessage DomElement not set
     */
    public function testInitWithoutDomElement() {
        $this->portail->init();
    }
    
    public function testInit() {
        $expectedId = "home";
        $expectedPortails = array("under-home1" => Null, "under-home2" => Null);
        
        $this->portail->setDomElement($this->dom->getElementById($expectedId));
        $this->portail->init();
        
        $this->assertEquals($expectedId, $this->portail->getId());
        $this->assertEquals($expectedPortails, $this->portail->getChildPortails());
    }
    
    /**
     * @depends testInit
     * @expectedException Exception
     */
    public function testGetChilWithWrongID() {
        $expectedId = "about";
        $this->portail->setDomElement($this->dom->getElementById("home"));
        $this->portail->init();
        
        $childPortail = $this->portail->getChildPortailById($expectedId);
    }
    
    /**
     * @depends testInit
     */
    public function testGetChildWithGoodID() {
        $expectedId = "under-home1";
        $expectedTitle = "home1";
        $expectedPortails = array("underunder-home1" => Null);
        $this->portail->setDomElement($this->dom->getElementById("home"));
        $this->portail->init();
        
        $childPortail = $this->portail->getChildPortailById($expectedId);
        
        $this->assertInstanceOf("Yeticms\XmlStorage\Portail", $childPortail);
        $this->assertEquals($expectedId, $childPortail->getId());
        $this->assertEquals($expectedTitle, $childPortail->getTitle());
        $this->assertEquals($expectedPortails, $childPortail->getChildPortails());
    }

    public function testGetAllTheTree() {
        $expectedId = "home";
        
        $this->portail->setDomElement($this->dom->getElementById($expectedId));
        $deepInit = true;
        $this->portail->init($deepInit);
        $arrayPortails = $this->portail->getChildPortails();
        $portailUnderHome1 = $arrayPortails["under-home1"];
        $portailUnderHome2 = $this->portail->getChildPortailById("under-home2");
        $arrayPortailsUnder1 = $portailUnderHome1->getChildPortails();
        $portailUnderUnderHome1 = $arrayPortailsUnder1["underunder-home1"];
        
        $this->assertEquals($expectedId, $this->portail->getId());
        $this->assertEquals("under-home1", $portailUnderHome1->getId());
        $this->assertEquals("under-home2", $portailUnderHome2->getId());
        $this->assertEquals("underunder-home1", $portailUnderUnderHome1->getId());
        $this->assertEquals(array(), $portailUnderUnderHome1->getChildPortails());
    }
    
    /**
     * @depends testGetAllTheTree
     */
    public function testUpdateDomElement() {
        $expectedId = "home";
        $expectString = '<portail id="under-home111">
                                                    <title><multilanguagestring><en>home1</en><fr>accueil1</fr></multilanguagestring></title>
                                                    <portail id="underunder-home1">
                                                        <title><multilanguagestring><en>home1</en><fr>accueil1</fr></multilanguagestring></title>
                                                    </portail>
                                                </portail>';
        
        $this->portail->setDomElement($this->dom->getElementById($expectedId));
        $deepInit = true;
        $this->portail->init($deepInit);
        $childPortail = $this->portail->getChildPortailById("under-home1");
        $childPortail->setId("under-home111");
        $deepUpdate = true;
        $this->portail->updateDomElement($deepUpdate);
        
        $this->assertFalse(!strpos(trim($this->dom->saveXML()), trim($expectString)));
    }
}