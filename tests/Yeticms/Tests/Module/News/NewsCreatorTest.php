<?php

namespace Yeticms\Tests\Module\News;

use Yeticms\Module\News\NewsCreator;

class NewsCreatorTest extends \PHPUnit_Framework_TestCase
{
    protected $newsCreator;
    
    public function setUp() {
        $this->newsCreator = new NewsCreator();        
    }

    public function testCreateDescription() {
        $content = $this->newsCreator->create();
        $this->assertInstanceOf("Yeticms\Module\News\News", $content);     
    }
}

