<?php

namespace Yeticms\Tests\Module\Description;

use Yeticms\Module\Description\DescriptionCreator;

class DescriptionCreatorTest extends \PHPUnit_Framework_TestCase
{
    protected $descriptionCreator;
    
    public function setUp() {
        $this->descriptionCreator = new DescriptionCreator();  
        $this->descriptionCreator->setDependencies(array("multilangue" =>  $this->getMock("Bxav\Util\Languages\MultiLanguage")));
    }

    public function testCreateDescription() {
        $content = $this->descriptionCreator->create();
        $this->assertInstanceOf("Yeticms\Module\Description\Description", $content);     
    }
}

