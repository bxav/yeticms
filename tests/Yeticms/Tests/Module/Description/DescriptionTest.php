<?php

namespace Yeticms\Tests\Module\Description;

use Yeticms\Module\Description\Description;
use Bxav\Util\Languages\MultiLanguage;

class DescriptionTest extends \PHPUnit_Framework_TestCase
{
    protected $xmlContentsString = '<?xml version="1.0"?>
                                            <!DOCTYPE contents [
                                            <!ELEMENT contents   (content+)>
                                            <!ATTLIST content    id           ID     #IMPLIED>
                                            ]>
                                        <contents>
                                            <content id="home" type="description">
                                                <text><multilanguagestring><en>home</en><fr>accueil</fr></multilanguagestring></text>
                                            </content>
                                        </contents>
                                        ';
    
    
    protected $multiLanguage;
    
    protected $dom;
    
    protected $description;
    
    protected $element = null;
  
    public function setUp() {
        $this->multiLanguage = new MultiLanguage(array(
            "languages" => array("en", "fr"), 
            "local" => "en"
        ));
        $this->dom = new \DOMDocument();
        $this->dom->loadXML($this->xmlContentsString);
        $element = $this->dom->getElementById("home");  
        $this->description = new Description();
        $this->description->setDomElement($element);
        $this->description->setMultilanguage($this->multiLanguage);
    }

    public function testInit() {
        $expectedId = "home";
        $expectedTexts = array("en" => "home", "fr" => "accueil");
        
        $this->description->init();
        
        $this->assertEquals($expectedId, $this->description->getId());
        $this->assertEquals($expectedTexts["en"], $this->description->getText());
        $this->multiLanguage->setLocal("fr");
        $this->assertEquals($expectedTexts["fr"], $this->description->getText());
    }
}

