<?php

/**
 * @author Xavier b <xavier.b@zoho.com>

 
namespace Yeticms\Tests;

use Silex\WebTestCase;

class YeticmsTest extends WebTestCase
{
    public function createApplication()
    {
        $app = require __DIR__.'/../../../src/Yeticms/app.php';
        return $app;
    }
    
    public function testTestPage() {
        $client = $this->createClient();
        $crawler = $client->request('', '/test');
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertCount(1, $crawler->filter('test'));
    }

}
 */