<?php

namespace Yeticms\Tests\Model;

use Yeticms\Util\Template\TemplateFactory;

class TemplateFactoryTest extends \PHPUnit_Framework_TestCase
{
    protected $templateFactory = null;

    public function setUp() {
        $templates = array("description" => $this->getMockTemplate(),
                           "news" => $this->getMockTemplate());
        $this->templateFactory = new TemplateFactory($templates, new \stdClass);
    }
    
    public function testGetWithGoodKey() {
        $template = $this->templateFactory->getTemplate("description");
        $this->assertInstanceOf("Yeticms\Util\Template\Template", $template);
    }
    
    /**
     * @expectedException Exception
     * @expectedExceptionMessage Template Key not registered
     */
    public function testGetWithWrongKey() {
        $template = $this->templateFactory->getTemplate("about");
    }
    
    private function getMockTemplate() {
        $stub = $this->getMockForAbstractClass('Yeticms\Util\Template\Template');
        return $stub;
    }
    
}