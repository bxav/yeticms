<?php

// configure your app for the production environment
use Symfony\Component\Translation\Loader\YamlFileLoader;


//Twig/////////////////////////
$app['twig.path'] = array(__DIR__.'/../templates');
$app['twig.options'] = array('cache' => __DIR__.'/../cache/twig');
$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
    // add custom globals, filters, tags, ...

    return $twig;
}));


//Translation//////////////////
$app['local_fallback'] = 'fr';
$app['translator']->setLocale($app['local_fallback']);
$app['translator'] = $app->share($app->extend('translator', function($translator, $app) {
    $translator->addLoader('yaml', new YamlFileLoader());
    $translator->addResource('yaml', __DIR__.'/locales/en.yml', 'en');
    //$translator->addResource('yaml', __DIR__.'/locales/de.yml', 'de');
    $translator->addResource('yaml', __DIR__.'/locales/fr.yml', 'fr');
    
    return $translator;
}));

//Mail/////////////////////////
$app['swiftmailer.options'] = array(
    'host' => 'host',
    'port' => '25',
    'username' => 'username',
    'password' => 'password',
    'encryption' => null,
    'auth_mode' => null
);

//Doctrine///////////////////
$app['dbs.options'] = array(
    'driver'    => 'pdo_mysql',
    'dbname'    => 'silex',
    'user'      => 'root',
    'password'  => 'walloo'
);

$app['orm.proxies_dir'] =  __DIR__.'/../cache/proxies';
$app['orm.em.options'] = array(
    "mappings" => array(
        array(
            "type" => "xml",
            "namespace" => "Yeticms\Entities",
            "path" => __DIR__."/config/mappings",
        )
    )
);







